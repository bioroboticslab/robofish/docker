c.NotebookApp.allow_root = True
c.NotebookApp.terminado_settings = {"shell_command": ["/bin/bash", "-l"]}
c.MultiKernelManager.default_kernel_name = "default"
c.NotebookApp.notebook_dir = "/"
c.LatexConfig.latex_command = "pdflatex"
